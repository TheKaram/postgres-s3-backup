#!/bin/bash

S3_ENDPOINT="ENDPOINT"
S3_BUCKET_NAME="BUCKET"
DB_NAME="NAME"
ENCRYPTION_KEY="VERY_SECRET"
EMAIL="YOUR@EMAIL.COM"

OUTPUT_FILE="${DB_NAME}_$(date +%Y-%m-%d[%H:%M:%S]).dump"

backup_database() {
    # Here is the dump command, edit it as you like
    # sudo -u postgres -i pg_dump -Z9 -Fc $DB_NAME -f $OUTPUT_FILE
    pg_dump -Z9 -Fc $DB_NAME -f $OUTPUT_FILE
    
    # Encrypt dump file
    ccencrypt $OUTPUT_FILE -K $ENCRYPTION_KEY
    # Upload encrypted dump file to S3
    aws --endpoint $S3_ENDPOINT s3 cp "${OUTPUT_FILE}.cpt" "s3://${S3_BUCKET_NAME}/${OUTPUT_FILE}.cpt"
    
    # If upload fails then send email notification
    if [ $? -ne 0 ]; then
            # Replace this with sending email script
            echo -en "Email Content" | mail -s "Backup Database Failed" $EMAIL
            echo "Failed"
    else
        rm "${OUTPUT_FILE}.cpt"
    fi
}

set_lifecycle_policy() {
    LIFECYCLE_CONFIG="$(mktemp)"
    if [ -z "$1" ]
    then
        # Default policy is to delete files after 2 days
        cat > "$LIFECYCLE_CONFIG" << EOF
{
    "Rules": [
        {
            "ID": "Delete old files",
            "Expiration": { "Days": 2 },
            "Filter": {},
            "NoncurrentVersionExpiration": { "NoncurrentDays": 2 },
            "Status": "Enabled"
        }
    ]
}
EOF
    else
        # Policy is to delete files after X days
        cat > "$LIFECYCLE_CONFIG" << EOF
{
    "Rules": [
        {
            "ID": "Delete old files",
            "Expiration": { "Days": $1 },
            "Filter": {},
            "NoncurrentVersionExpiration": { "NoncurrentDays": $1 },
            "Status": "Enabled"
        }
    ]
}
EOF
    fi
    aws --endpoint $S3_ENDPOINT s3api put-bucket-lifecycle-configuration \
        --bucket "${S3_BUCKET_NAME}" \
        --lifecycle-configuration "file://${LIFECYCLE_CONFIG}"
}

case "$1" in
    backup)
        backup_database
        ;;
    set-lifecycle)
        set_lifecycle_policy $2
        ;;
    *)
        echo "Usage: $0 {backup|set-lifecycle [int, default: 2]}"
        exit 1
esac