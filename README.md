# postgres-s3-backup

## Requirements
1. PostgreSQL 🙃
2. [AWS CLI](https://aws.amazon.com/cli/)
3. `ccrypt`, used to encrypt database
4. `mailutils`, used to send email notifications when the upload fails
## How to Install
just run 
```shell
$ git clone https://codeberg.org/TheKaram/postgres-s3-backup.git
$ cd postgres-s3-backup
$ chmod +x backup-db.sh
```

Then Open `backup-db.sh` and set the required variables.

## Set Lifecycle for bucket
if you want to delete all backups older than 1, 2, 3 .. days you can use `set-lifecycle`.

ex: set lifecycle for 3 days:

```shell
$ ./backup-db.sh set-lifecycle 3
```

## Automate backup process
You can use `cron` to automate the backup process if you are using Linux.
This example cron file backup databse every 1 hour, save this file to `/etc/cron.d/backup-db`.

```
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

0 */1 * * * karam bash /home/karam/postgres-s3-backup/backup-db.sh

```
Don't forget to replace karam with your own username and to change path if needed.